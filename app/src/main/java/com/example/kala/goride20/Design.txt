Kaloyan A. Baltiev

kaloyan.baltiev@gmail.com



Design of android application GO RIDE 2.0

The application runs on a minimum Android API 17. It is a GPS tracker that saves user's
coordinates and plots them on a google map.

The core of the app is it's GPS tracker. It uses LocationManager class, with  ACCESS_FINE_LOCATION
permissions for maximum accuracy. The GPS_Service class broadcasts location (no bound) since
it extends service it can be used in the background.

The Main Activity checks the permissions for SDK >= 23. It also sets a broadcast filter. The last
function that it does is to display coordinates , and save them in to a SQLite database on the users
devise among with input name from the user. The GeoPoint class encapsulates this information.

Next is the DBHandler. It connects the app to the database. It creates a table with four columns:
id, latitude, longitude and name. It has build in functionality to C.U.R.D. Reading is implemented
in two functions. The first is reading all the names of the coordinates and adding new entries in
an ArrayList, which is later displayed. The second is by passing one name adding all the geopoints
with this name to a LatLnd ArrayList. It is fair to say that the database can be optimised by
splitting it into two tables but since this was my first android project i totally ran out of time
to do it.

LibraryOfMapsActivity takes care of displaying the list of saved maps to the user. It can delete a map
by name or view it by name. This name is typed by the user and passed along the intend to open google
map. There are some error checking for correct input. In the MapActivity the name is red and
passed as argument to the read method that gets the geopoints from the database. The first
point is used as a reference to the camera and the zoom level is set to 15.


